var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); 

module.exports = {
  context: __dirname,
  devtool: "source-map",
  entry: "./src/app.js",
  output: {
    path: __dirname + "/dist",
    filename: "bundle.js",
    publicPath: '/'
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 8080
  },
  plugins: [  // Array of plugins to apply to build chunk
    new HtmlWebpackPlugin({
        template: __dirname + "/public/index.html",
        inject: 'body'
    })
],
};
